Trabajo Práctico Entorno de la Programación

Integrantes:Geuna, Facundo; Texier, Julieta

Pasos a seguir

1. Clonamos el repositorio con todos los scripts: git clone https://gitlab.com/facugeuna/tp-entorno.git

2. Creamos la carpeta imagenes dentro del directorio donde se encuentra el dockerfile

3. Creamos la imagen con el comando: docker build -t tpentorno . (para correrlo debemos estar dentro de la carpeta donde se encuentra el Dockerfile)

4. Corremos la imagen construida con: sudo docker run -it -v ./imagenes:/root/tp-entorno/scripts/imagenes_descomprimidas tpentorno (todos los archivos y las imagenes que se creen dentro del dockerfile van a guardarse en la carpeta imagenes)

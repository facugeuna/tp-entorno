#falta completar los demas a medida q los vamos haciendo

echo "Elija una opción: "
echo "Opción 1: Generar imágenes."
echo "Opción 2: Descargar imágenes."
echo "Opción 3: Descomprimir imágenes."
echo "Opción 4: Procesar las imágenes."
echo "Opción 5: Comprimir las imágenes."
echo "Opción 6: Salir"
read opcion

if ! [[ $opcion =~ ^[1-6]$ ]]; then
 echo "Opción inválida. Ingrese una opción válida."
 ./menu.sh
 exit 1
fi

case $opcion in
 1)
 echo "Usted eligió la opción 1."
 echo "Ingrese la cantidad de fotos que desea generar: "
 read cantidad
 ./generar.sh $cantidad
 ./menu.sh
 ;;

 2)
 echo "Usted eligió la opción 2."
 echo "Ingrese el URL del archivo comprimido."
 read url_comprimido
 echo "Ingrese el URL de la suma de verificación."
 read suma_verificacion
 ./descargar.sh $url_comprimido $suma_verificacion
 ./menu.sh
 ;;

 3)
 echo "Usted eligió la opción 3."
 ./descomprimir.sh
 ./menu.sh 
 ;;

 4)
 echo "Usted eligió la opción 4."
 ./procesar.sh
 ./menu.sh
 ;;

 5)
 echo "Usted eligió la opción 5."
 ./comprimir.sh
 ./menu.sh
 ;;
 6)
 echo "Usted eligió salir."
 exit 0
esac 


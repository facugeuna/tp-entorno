DIRECTORIO=~/tp-entorno/scripts/imagenes_descomprimidas/

for IMAGEN in $DIRECTORIO/*; do
 NOMBRE_IMAGEN=$(basename -s .jpg $IMAGEN)
 if [[ $NOMBRE_IMAGEN =~ ^[A-Z][a-z]+_[A-Z][a-z]+ ]]; then
  convert $IMAGEN -resize 512x512+0+0 -extent 512x512 $IMAGEN
  echo "$NOMBRE_IMAGEN.jpg procesada."
 fi
done

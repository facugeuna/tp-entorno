URL_COMPRIMIDO=$1
URL_SUMA_VERIFICACION=$2


if [[ $# -ne 2 ]]; then
 echo "Debe ingresar solamente dos argumentos."
 exit 1
fi

if [[ ! -d ~/tp-entorno/scripts/imagenes_entorno/ ]]; then
 mkdir -p  ~/tp-entorno/scripts/imagenes_entorno/
fi

cd ~/tp-entorno/scripts/imagenes_entorno/

wget -O imagenes.zip $URL_COMPRIMIDO

wget -O suma_verificacion.txt $URL_SUMA_VERIFICACION

SUMA_VERIFICACION_COMPRIMIDO=$(sha256sum imagenes.zip | tr -d " ") 

SUMA_VERIFICACION_DESCARGAR=$(cat suma_verificacion.txt | tr -d " ")

cd ..

if [[ $SUMA_VERIFICACION_COMPRIMIDO == $SUMA_VERIFICACION_DESCARGAR ]]; then
 echo "Las sumas de verificacion son iguales."
else
 echo "ERROR. Las sumas de verificacion no coinciden."
 exit 2
fi


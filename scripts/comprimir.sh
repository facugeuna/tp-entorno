
DIRECTORIO=~/tp-entorno/scripts/imagenes_descomprimidas/
contador=0

for IMAGEN in $DIRECTORIO/*; do

 NOMBRE_IMAGEN=$(basename $IMAGEN)
 echo "$NOMBRE_IMAGEN" >> ~/tp-entorno/scripts/imagenes_descomprimidas/nombres_imagenes.txt

 NOMBRE_SOLO=$(basename -s .jpg $IMAGEN)
 if [[ $NOMBRE_SOLO =~ ^[A-Z][a-z]+_[A-Z][a-z]+ ]]; then
  echo "$NOMBRE_SOLO" >> ~/tp-entorno/scripts/imagenes_descomprimidas/nombres_validos.txt
 fi

 if [[ $NOMBRE_SOLO =~ a$ ]]; then
  contador=$((contador + 1))
 fi

done

echo "La cantidad de imágenes cuyo nombre finaliza con la letra a es:  $contador" >> ~/tp-entorno/scripts/imagenes_descomprimidas/finaliza_con_a.txt

zip -j  ~/archivos.zip $DIRECTORIO/*

if [[ -d ~/tp-entorno/scripts/nombres_imagenes/  ]]; then
 rm -r ~/tp-entorno/scripts/nombres_imagenes/
fi

#rm -r ~/tp-entorno/scripts/imagenes_descomprimidas ~/tp-entorno/scripts/imagenes_entorno 



CANT_IMAGENES=$1
IMAGENES="https://source.unsplash.com/random/900%C3%97700/?person."
NOMBRES="https://raw.githubusercontent.com/adalessandro/EdP-2023-TP-Final/main/dict.csv"

if [[ $# -ne 1 ]]; then
 echo "Debe ingresar solamente la cantidad de imagenes que desea generar en un argumento."
 exit 1
fi


if [[ $CANT_IMAGENES -eq 0 || ! $CANT_IMAGENES =~ ^[0-9]+$ ]]; then
 echo "Debe ingresar la cantidad de imágenes que desea generar."
 exit 2
fi

if [[ ! -d ~/tp-entorno/scripts/nombres_imagenes/ ]]; then
 mkdir -p  ~/tp-entorno/scripts/nombres_imagenes/
 cd ~/tp-entorno/scripts/nombres_imagenes/
 wget $NOMBRES
 cd ..

fi

if [[ ! -d ~/tp-entorno/scripts/imagenes_entorno/ ]]; then
 mkdir -p  ~/tp-entorno/scripts/imagenes_entorno/
fi

cd ~/tp-entorno/scripts/imagenes_entorno

for (( i=0; i < $CANT_IMAGENES; i++ ));
do
 NOMBRE_ALEATORIO=$(shuf -n "1" ~/tp-entorno/scripts/nombres_imagenes/dict.csv)
 NOMBRE_IMAGEN="$(echo "$NOMBRE_ALEATORIO" | cut -d "," -f 1).jpg"
 NOMBRE_IMAGEN=$(echo "$NOMBRE_IMAGEN" | tr ' ' '_' )

 wget -O $NOMBRE_IMAGEN $IMAGENES

 sleep 1

done


zip imagenes.zip *.jpg

sha256sum imagenes.zip > suma_verificacion.txt

rm -r *.jpg

echo "Imágenes generadas."

